# Dunst Centering Patch

Patch to allow Dunst notifications to be centered either horizontally or vertically on the screen.

Some of the code is based off the centering branch from the official repository. However, this branch is no longer maintained, which means you won't get the latest version of the software. Having the centering functionnality be patchable aims to fix that.

This patch is designed to work with Dunst 1.4.1.


# Installation

## Manual patching
First, get the Dunst 1.4.1 source files:
```
curl -OL https://github.com/dunst-project/dunst/archive/v1.4.1.tar.gz
tar xvzf v1.4.1.tar.gz
```

Apply the patch:
```
patch dunst-1.4.1/src/draw.c draw.c-1.4.1.patch
patch dunst-1.4.1/src/settings.c settings.c-1.4.1.patch
patch dunst-1.4.1/src/settings.h settings.h-1.4.1.patch
```

You can now ```cd``` into the *dunst-1.4.1* directory and run ```make``` to build Dunst!


## Arch Linux
A PKGBUILD is included, just run ```makepkg```. It'll build a *dunst-centering* package.


# Configuration

The patch adds a new *centering* option that you can use in your *dunstrc*. Possible values are:
```
centering = off         # Disables the centering. Vanilla Dunst behavior
centering = horizontal  # Centers notifications horizontally. x value of the geometry option is ignored
centering = vertical    # Centers notifications vertically. y value of the geometry option is ignored
centering = both        # Centers notifications on both axes. x and y values of the geometry option are ignored
```

You can also specify the centering value through command line with:
```
-centering value
```
